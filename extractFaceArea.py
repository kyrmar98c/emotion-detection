import cv2
import os
import glob

casc_pathface = r'C:\Python39\Lib\site-packages\cv2\data\haarcascade_frontalface_default.xml'
data_path=r'C:\emotion-detection\dataset\validation\surprise' + r'/*.*'
write_path=r'C:/emotion-detection/dataset/validation/faces/surprise/' 
image_number=1
image_list=glob.glob(data_path)
for file in image_list:
    image=cv2.imread(file,1)
    face_cascade = cv2.CascadeClassifier(casc_pathface)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces=face_cascade.detectMultiScale(gray,1.08,2 )
    for(x, y, w, h) in faces:
        print(write_path+str(image_number)+r'.jpg')
        roi_color=image[y:y+h, x:x+w]
        resized = cv2.resize(roi_color, (128, 128))
        cv2.imwrite(write_path+str(image_number)+r'.jpg',resized)
        image_number+=1
