# Instructions to run the code #
1. Align with the dependencies in Project.toml
2. For PyCall add PyCall Pkg.add("PyCall") and also Conda
    - Pkg.add("Conda")
    - using Conda
    - Conda.runconda(`install -c conda-forge opencv`)  (install opencv to conda integrated with julia) 

3. The main method has the experiments conducted for the thesis(they are commented out and in order as described in the thesis)
uncomment and run main to see the confusion matrix of the experiment.
4. Other than that the behavior of our classifier is described below. 


# emotion-detection

Emotion-detection is a project about detecting human emotion in static images. A back-propagation algorithm has been implemented for the training of the neural
networks. Also there is a module that has some implementations for the image preprocessing needed before the images come to a proper form to be feeded in the model.
The overall structure of the project is three modules. Back-propagation module, image preprocessing module and Artificial intelligence module. The experements described in the Thesis are executed in Main.jl .

## Back-Propagation

This module exports one function. The **calculate** function.

**calculate(weights, X, Y, activationFunctions, epochs)**

**inputs:**

**weights**: A vector of arrays which represents the weights of the network. Each array represents the connection of the previous layer to the next. Weights should be align with the input that is being feeded to the network and the expected output layer.

**X**: A vector of vectors with the inputs. It's a vector because the function support batch training with more than one input and output. Each vector represents one input sample.

**Y**: A vector of vectors with the outputs. It's a vector because the function support batch training with more than one input and output. Each vector represents one output sample.

**activationFunctions**: It's a vector of functions each one corresponding to the activation function that is to be used for each layer.

**epochs**: It's the number of iterations to be done before terminating the training of the model

**output**: void

## Image Preprocessing

This module exports one function. The **imTo1200Vec** function.

**imTo1200Vec(imgs)**

**inputs**:

**imgs**: The images to be proccessed.

**output**: The biniriazed vectors of that each is representing an image and are going to be fed to the neural network.

## AL

This module exports two functions. The **ANNCalssifier** and the **squareOfErrors**.

**squareOfErrors(yA, yP)**

**inputs**:

**yA**: The actual result in the form of a vector.

**yP**: The predicted result in the form of a vector.

**output**: The error calculated acccording to the Mean squared prediction error formula.


**ANNCalssifier()**

**inputs**:

This function does not accept any input.

**output**:

This function does not have any output.

This function is meant to be used as an object. It has two variables declared inside of it. The weights which represent the weights of the network and the activationFunctions which represent the activationFunctions. This function has also some other functions inside of it which can be invoked and they do produce an output or they update the two variables this function has. 

**setActivationFunctions(activationFunction_)**:

This method sets the value activationFunctions to the activationFunction_.

**getActivationFunction()**

Returns the activation functions if the function.

**initWeights(weights_)**

This function takes as input a vector with the size of each layer and creates the arrays that represents the weights acccordingly. However it does not set the weights that connect the input layer to the first hidden layer and the weights that connect the last hidden layer to the output layer. For that the fit function needs to be called.

**getWeigths()**

This function gets the weights of the function.

**fit(inputX_,inputY_)**

This function sets the the weights that connect the input layer to the first hidden layer and the last hidden layer to the output layer according to the size of inputX_ and inputY_ respectively.

**train(X_,Y_,a_,epochs_)**

This function takes as inputs two vectors of number vectors X_ and Y_ that represent the inputs and the outputs respectively, the learning rate a_, and the epochs_ and trains the model updating the weights.

**predict(X_)**

This function accepts an input and predicts its value. 

**predict_proba(X_)**

This functions accepts an input and predicts the propability of each case.

## Example

    patchX = Vector{AbstractVector{<:Number}}() 

    patchY = Vector{AbstractVector{<:Number}}()

    for i in range(1,10)

        push!(patchX,rand(-1.0:0.00000001:1.0,1,10))

        push!(patchY,[1,0])
    end

    for i in range(1,10)

        push!(patchX,rand(-1.0:0.00000001:1.0,1,10))

        push!(patchY,[0,1])
    end

    classifier = AL.ANNClassifier()

    classifier.setActivationFunctions([relu,σ])

    classifier.initWeights([20])

    classifier.fit(patchX[1],patchY[1])

    classifier.train(patchX,patchY,0.01,100) 

    classifier.predict([-0.2, 0.4, -0.2 , 0.34, -0.23, 0.23, 0.24, -0.78, 0.32, 0.18])




## Experiments 

The expreriments were conducted using the parametrized function Experiment.performTestExperiment(). All arguments are optional but 
some arguments are expected to be properly set for example the layers along with the provided activationFunctions:

**Parameters: **

emotions: vector of enum type Expiriment.Emotion for example `[Experiment.happiness, Experiment.anger, Experiment.surprise, Experiment.sadness]` , tells the function to train model for these four emotion(classes)


trainer: enum type Expiriment.TrainerType. Specifies if the custom classifier created for thesis will be used or Flux. For exampl Expiriment.custom

model: enum type Expiriment.ModelType. Specifies if one model is to be used or a committee of seven models that vote for the class. For example Expiriment.standard

weights: vector of Integer. Specifies the size of each hidden layer(the input and the output are automatically calculated). For example [600] means a neural network with one hidden layer.

activationFunctions: vector of Function. Specifies the activation functions for each layer. Should be the size of weights + 1. for example [Flux.σ,Flux.relu]

epochs: Integer. Specifies the iterations for the training of the model

batchSize: Integer. Specifies the batch size to be used in the model training


**Output** 
Prints a confusion matrix


**Example**

    Experiment.performTestExperiment(;emotions =[Experiment.happiness, Experiment.anger, Experiment.surprise, Experiment.sadness], 
    activationFunctions = [relu, σ], weights = [600], epochs = 25, batchSize = 200, trainer = Experiment.fluxx)

    prints 

    
![confusion matrix for the given parameters](https://gitlab.com/kyrmar98c/emotion-detection/-/blob/main/img/confusionmatrix.png)


