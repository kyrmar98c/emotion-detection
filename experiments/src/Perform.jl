using StatsBase
using Images
using Flux
using Reduce
using GLMakie
using Lighthouse: confusion_matrix, plot_confusion_matrix

include("LoadImages.jl")
include("../../AL/AL.jl")

## enumaration for the emotions ##
@enum Emotion happiness anger surprise disgust fear neutral sadness

## specifies the type of the final model ##
@enum ModelType standard committee

## specifies the type of the trainer to be used ## 
@enum TrainerType fluxx custom

defaultTrainer = custom
defaultModel = standard
defaultEmotions = [happiness,anger,surprise,disgust,fear,neutral,sadness]
defaultActivationFunction = Flux.σ
defaultWeights = [800]
defaultEpochs = 30
defaultBatchSize = 200
defaultLearningRate = 0.05
datapathTrain = "dataset/train/faces/"
datapathTest = "dataset/validation/faces/"

function performTestExperiment(;trainer::TrainerType = defaultTrainer ,model::ModelType =defaultModel ,epochs::Int64 = defaultEpochs,emotions::Vector{Emotion} = defaultEmotions 
    ,activationFunctions::Vector{Function} = [defaultActivationFunction,defaultActivationFunction],weights::Vector{Int64} = defaultWeights,batchSize::Int64 = defaultBatchSize, 
    learningRate:: Float64 = defaultLearningRate) 

    if (fluxx == trainer && standard == model)
        fluxModelExperiment(emotions, activationFunctions, weights, batchSize, epochs,learningRate)
    elseif (fluxx == trainer && committee == model)
        modelCommiteeFlux(emotions, activationFunctions, weights, batchSize, epochs,learningRate)
    elseif (custom == trainer && standard == model)
        customModelExperiment(emotions, activationFunctions, weights, batchSize, epochs,learningRate)
    else
        modelCommiteeCustom(emotions, activationFunctions, weights, batchSize, epochs,learningRate)
    end
     
end

function tostring(val::Emotion) 
    if (val == happiness)
        "happiness"
    elseif (val == anger)
        "anger"
    elseif (val == surprise)
        "surprise"
    elseif (val == disgust)
        "disgust"
    elseif (val == fear)
        "fear"
    elseif (val == neutral)
        "neutral"
    else
        "sadness"
    end
     
end

function loadPaths(emotions::Array{Emotion}, base::String) 
    return map((x)->readdir(base .* tostring.(x) .* "/" , join=true),emotions)
end


function customModelExperiment(emotions::Vector{Emotion}, activationFunctions::Vector{Function}, weights::Vector{Int64}, batchSize::Integer, epochs::Integer, 
    learningRate::Float64; printMatrix::Bool = true)

    ## Load train data ##
    traindatapaths = loadPaths(emotions, datapathTrain)
    traindata = map((x) -> loadAndConvertImagesNew(sample(x,batchSize,replace=true)), traindatapaths)


    #patches 
    patchX = Vector{AbstractVector{<:Number}}()

    patchY = Vector{AbstractVector{<:Number}}()
    
    # create classifier
    classifier = AL.ANNClassifier()
    
    #set activationFunction
    classifier.setActivationFunctions(activationFunctions)
                
    # initialize weights specifing the size of the hidden layer
    # this will result in a ANN with one hiden and one output layer
    classifier.initWeights(weights)
    global counter = 1
    for emotion in traindata
        encodedY = vec(zeros(1,size(emotions,1)))
        encodedY[counter] = 1
        for image in emotion
            push!(patchX, image)
            push!(patchY, encodedY)
        end
        counter = counter + 1
    end

        
    classifier.fit(patchX[1], patchY[1])    
    classifier.train(patchX, patchY, learningRate, epochs)

    ## Load test data ##
    testdatapaths = loadPaths(emotions, datapathTest)
    testdata = map((x) -> loadAndConvertImagesNew(x), testdatapaths)

    classes = map((x)->tostring(x),emotions)
    ground_truth =     []
    predicted_labels = []

    global counter = 1
    for emotion in testdata
        for image in emotion
            prediction = classifier.predict(image)
            push!(ground_truth,counter)
            push!(predicted_labels,prediction)
         end
        counter = counter + 1
    end

    if  (printMatrix)
        confusion = confusion_matrix(length(classes), zip(predicted_labels, ground_truth))
        fig, ax, p = plot_confusion_matrix(confusion, classes, :Column)
        display(fig)
    end
    return classifier

end

function fluxModelExperiment(emotions::Vector{Emotion}, activationFunctions::Vector{Function}, weights::Vector{Int64}, batchSize::Integer, epochs::Integer,
     learningRate::Float64; printMatrix::Bool = true)
    ## Load train data ##
    traindatapaths = loadPaths(emotions, datapathTrain)
    traindata = map((x) -> loadAndConvertImagesNew(sample(x,batchSize,replace=true)), traindatapaths)

    inputSize = reduce(+,map(x->size(x,1),traindata))
    patchX = zeros(size(traindata[1][1],1),inputSize)
    patchY = zeros(size(emotions,1),inputSize)

    global counter = 1
    global step = 0
    for emotion in traindata
        encodedY = vec(zeros(1,size(emotions,1)))
        encodedY[counter] = 1
        for i in range(1,size(emotion,1))
            patchX[:,i+step] = emotion[i]
            patchY[:,i+step] = encodedY
        end
        counter = counter + 1
        step = step + size(emotion,1)
    end

    
    data= Flux.Data.DataLoader((patchX,patchY), batchsize=batchSize, shuffle=true)
    layers = []
    push!(layers,Dense(size(traindata[1][1],1),weights[1],activationFunctions[1]))
    for i in range(2,size(weights,1))
        push!(layers,Dense(weights[i-1],weights[i],activationFunctions[i]))
    end
    push!(layers,Dense(weights[end],size(emotions,1),activationFunctions[end]))
    classifier = Chain(layers)

    opt = Descent(learningRate)
    
    loss(x, y) = sum(Flux.Losses.binarycrossentropy(classifier(x), y))

    ps = Flux.params(classifier)
    
    for i in 1:epochs
        Flux.train!(loss, ps, data, opt)
    end

    ## Load test data ##
    testdatapaths = loadPaths(emotions, datapathTest)
    testdata = map((x) -> loadAndConvertImagesNew(x), testdatapaths)
    
    classes = map((x)->tostring(x),emotions)
    ground_truth =     []
    predicted_labels = []

    global counter = 1
    for emotion in testdata
        for image in emotion
            prediction = argmax(classifier(image))
            push!(ground_truth,counter)
            push!(predicted_labels,prediction)
         end
        counter = counter + 1
    end
    
    if (printMatrix==true)
        confusion = confusion_matrix(length(classes), zip(predicted_labels, ground_truth))
        fig, ax, p = plot_confusion_matrix(confusion, classes, :Column)
        display(fig)
    end
    return classifier

end

function modelCommiteeFlux(emotions, activationFunctions, weights, batchSize, epochs,learningRate)

    classifiers = []

    for i in range(1,7)
        push!(classifiers,fluxModelExperiment(emotions, activationFunctions, weights, batchSize, epochs,learningRate;printMatrix=false))
    end

    ## Load test data ##
    testdatapaths = loadPaths(emotions, datapathTest)
    testdata = map((x) -> loadAndConvertImagesNew(x), testdatapaths)

    classes = map((x)->tostring(x),emotions)
    ground_truth =     []
    predicted_labels = []

    global counter = 1
    for emotion in testdata
        for image in emotion
            poll = vec(zeros(size(emotions,1),1))
            map((x)-> poll[argmax(x(image))]=poll[argmax(x(image))]+1,classifiers)
            prediction = argmax(poll)
            push!(ground_truth,counter)
            push!(predicted_labels,prediction)
            end
        counter = counter + 1
    end
    
    confusion = confusion_matrix(length(classes), zip(predicted_labels, ground_truth))
    fig, ax, p = plot_confusion_matrix(confusion, classes, :Column)
    display(fig)


end

function modelCommiteeCustom(emotions, activationFunctions, weights, batchSize, epochs,learningRate)
    classifiers = []

    for i in range(1,7)
        push!(classifiers,customModelExperiment(emotions, activationFunctions, weights, batchSize, epochs,learningRate;printMatrix=false))
    end

    ## Load test data ##
    testdatapaths = loadPaths(emotions, datapathTest)
    testdata = map((x) -> loadAndConvertImagesNew(x), testdatapaths)

    classes = map((x)->tostring(x),emotions)
    ground_truth =     []
    predicted_labels = []

    global counter = 1
    for emotion in testdata
        for image in emotion
            poll = vec(zeros(size(emotions,1),1))
            map((x)-> poll[argmax(x.predict(image))]=poll[argmax(x.predict(image))]+1,classifiers)
            prediction = argmax(poll)
            push!(ground_truth,counter)
            push!(predicted_labels,prediction)
            end
        confusionMatrix[:,counter] = confusionMatrix[:,counter] ./size(emotion,1)
        counter = counter + 1
    end
    
    confusion = confusion_matrix(length(classes), zip(predicted_labels, ground_truth))
    fig, ax, p = plot_confusion_matrix(confusion, classes, :Column)
    display(fig)

    

end
