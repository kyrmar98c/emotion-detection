
# module 

module CalculateWeights
using Calculus

export calculate



function calculate(weights,X,Y,a,activationFunctions,epochs)

    b= [0,0]

    # initial feedforward 
    O=initiliazeNodes(X,weights,activationFunctions,b)

    # update weights backpropagating
    return updateWeights(O,activationFunctions,epochs,weights,Y,a,b)

end

function initiliazeNodes(X,weights,activationFunction,b)
    O=Vector{Vector{Vector{<:Number}}}()
    push!(O,X)
    # calculate each layer for each batch
    for i in range(1,size(weights,1))
        push!(O,vectorizeVectorOfVectors(O[end],weights[i],activationFunction[i],b[i]))
    end
    return O
end


function vectorizeVectorOfVectors(vectorOfVectors,arrayToMultiple,activationFunction,b)
    result=Vector{Vector{<:Number}}()
    # loop through all batches 
        for i in vectorOfVectors
            push!(result,activationFunction.(vec((i'*arrayToMultiple).+b)))
        end
    return result
end

function updateWeights(nodes,activationFunctions,epochs,weights,Act,a,b)

    copyWeights=weights
    O=nodes
    derActivationFunctions=derivative.(activationFunctions)
    batchSize=length(O[end])
    chainRule=Vector{Array{<:Number}}()
    totalNodes=length(O)
    for i in range(1,batchSize)
    push!(chainRule,[1 2;3 4])
    end
    for i in range(1,epochs)
        print(i)
        dEdW=zeros(size(copyWeights[end],1),size(copyWeights[end],2))
        for j in range(1,batchSize)
            #dEdW+=((O[end-1][j])*((-2*(Act[j].-O[end][j]).*((derActivationFunction.(O[end-1][j]'*copyWeights[end].+b[end]))')))')/length(O[end][j])
            dEdW+=((O[end-1][j])*((-2*(Act[j].-O[end][j])))')/length(O[end][j])
        end
        dEdW/=length(O[end])

        if (sqrt(sum(dEdW.^2)) < 0.01) 
            break
        end

        derActivationFunction=derActivationFunctions[end]
        for j in range(1,batchSize)
           #chainRule[j]=(-2*(Act[j]-O[end][j])'.*derActivationFunction.(O[end-1][j]'*copyWeights[end].+b[end]))*copyWeights[end]'/length(O[end][j])
           chainRule[j]=(reshape((-2*(Act[j]-O[end][j])),size(Act[j],2),size(Act[j],1   )))*copyWeights[end]'/length(O[end][j])
        end
        copyWeights[end]-=a*dEdW
        
        for  j in  reverse(range(2,length(O)-1))
            dEdW=zeros(size(copyWeights[j-1],1),size(copyWeights[j-1],2))
            for k in range(1,batchSize)
                derActivationFunction=derActivationFunctions[j-1]
                dEdW+=((chainRule[k].*derActivationFunction.(O[j-1][k]'*copyWeights[j-1].+b[j-1]))'*O[j-1][k]')'
                chainRule[k]=(chainRule[k].*(derActivationFunction.(O[j-1][k]'*copyWeights[j-1].+b[j-1])))*copyWeights[j-1]'
            end
            dEdW/=length(O[end])
            copyWeights[j-1]-=a*dEdW
        end
        for j in range(1,totalNodes-1)
            activationFunction=activationFunctions[j]
            for k in range(1,batchSize)
                 O[j+1][k]=activationFunction.(vec(O[j][k]'*copyWeights[j].+b[j]))
            end
        end
    end 
    
    return copyWeights

end


end