module Process

using PyCall
using Images
using ImageView
using UUIDs
using Random
using Statistics

py"""
import cv2
import dlib

def extract_eye_mouth_regions(imgPaths):
    regions = []
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    counter=1
    for imgPath in imgPaths:
        img = cv2.imread(imgPath)
        gray = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2GRAY)
        faces = detector(gray)
        if not faces :
            faces = detector(gray)
        for face in faces:
            landmarks = predictor(image=gray, box=face)
            x1 = landmarks.part(17).x
            y1 = landmarks.part(19).y
            x2 = landmarks.part(26).x
            # eyes region
            y2 = landmarks.part(29).y
            regions.append([x1,y1,x2,y2])
            # mouth region
            y2 = landmarks.part(57).y
            y1 = landmarks.part(3).y
            regions.append([x1,y1,x2,y2])
    return regions
"""
function binarize(x,thresshold)
    if(x<thresshold)
        return 1
    end
    return 0
end

function binarizeAlt(x,thresshold)
if(x<thresshold)
   return RGB{N0f8}(0.0,0.0,0.0)
end
return RGB{N0f8}(1.0,1.0,1.0)
end

function negativeValues(val)
    if(val<1) 
        return 1
    end
    if(val>128)
        return 128
    end
    return val       
end

function imTo1200Vec(imgs)
uniquePaths = []
i=0
for img in imgs
    rng = MersenneTwister(i)
    i=i+1
    uniquePath = "tmp/" * string(UUIDs.uuid4(rng).value) * ".jpg"
    save(uniquePath, img)
    push!(uniquePaths,uniquePath)
end
regions = py"extract_eye_mouth_regions"(uniquePaths)
for uniquePath in uniquePaths
    rm(uniquePath)
end
binarizedOutputs = []
i=1
for img in imgs

    grayImg = Gray.(img)

    eyesRegion = grayImg[negativeValues(regions[i,2]):negativeValues(regions[i,4]),negativeValues(regions[i,1]):negativeValues(regions[i,3])]

    moutRegion = grayImg[negativeValues(regions[i+1,2]):negativeValues(regions[i+1,4]),negativeValues(regions[i+1,1]):negativeValues(regions[i+1,3])]

    merged = vcat(eyesRegion,moutRegion)

    merged = imresize(merged,(80,60))

    sortedM = sort(vec(merged))

    thresshold = sortedM[2200]

    #imshow(binarize.(merged,thresshold))

    elmerged = binarizeAlt.(merged,thresshold)

    push!(binarizedOutputs,vec(binarize.(merged,thresshold)))

    i=i+2

end 

return binarizedOutputs

end
end
