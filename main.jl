using Flux

include("experiments/Experiment.jl")

begin

    Experiment.performTestExperiment(;emotions =[Experiment.happiness, Experiment.anger, Experiment.surprise, Experiment.sadness], 
    activationFunctions = [relu, σ], weights = [600], epochs = 10, batchSize = 10, trainer = Experiment.fluxx, model = Experiment.committee)

end


