using DelimitedFiles
# Module to create multilayer neural network
include("../../BackPropagation/BackPropagation.jl")
include("PredictY.jl")
# Specify generic activation functions
sigmaAbs(x)=(x>=0)*1
sigma(x)=1/(1+exp(-x))


function initiliazeWeights(x)
    weights= Vector{AbstractArray{<:Number}}()

    push!(weights,rand(Float64,(1,x[1])))

    for i in 1:length(x)-1
        push!(weights,rand(-1.0:0.00000001:1.0,(x[i],x[i+1])))
    end

    push!(weights,rand(Float64,(x[end],1)))

    return weights

end

function fitWeights(inputX,outputY,weights)
    weightsLenght=length(weights)
    firstWeights=rand(-1.0:0.00000001:1.0,(length(inputX),size(weights[2],1)))
   # for i in range(1,size(weights[2],1))
    #    firstWeights[:,i]/=sum(firstWeights[:,i])
    #end
    weights[1]=firstWeights
    lastWeights = rand(-1.0:0.00000001:1.0,(size(weights[weightsLenght-1],2),length(outputY)))
    #for i in range(1,length(outputY))
     #   lastWeights[:,i]/=sum(lastWeights[:,i])
    #end
    weights[weightsLenght]=lastWeights
    return weights
end

#train the Classifier
# X a vector of vectors each of which represent n different inputs
# Y a vector of vector each of which represent n different outputs
function trainModel(weights,X,Y,a,activationFunctions,epochs)
    return BackPropagation.CalculateWeights.calculate(weights,X,Y,a,activationFunctions,epochs)
end

# Predict the propabilities of each class
function predict_probas(weigths,X,activationFunctions)
    return PredictY.predictY(weigths,X,activationFunctions)
end

# Predict class
function predictv(weigths,X,activationFunction)
    Y=PredictY.predictY(weigths,X,activationFunction)
   return argmax(vec(Y)) 
end

function exportv(weights)
    writedlm("1.txt", weights[1])
    writedlm("2.txt", weights[2])
end

function importv()
    layer = readdlm("1.txt")
    weights::Vector{AbstractArray{<:Number}}= Vector{AbstractArray{<:Number}}()
    push!(weights, layer)
    layer = readdlm("2.txt")
    push!(weights, layer)
    return weights
end

# Function to be returned when Classifier is built
function ANNClassifier()
    
    # The weights of the network    
    weights::Vector{AbstractArray{<:Number}}= Vector{AbstractArray{<:Number}}()

    # The activation function to be used
    activationFunctions::Vector{Function} = [x->x]

    setActivationFunctions(activationFunction_)=( activationFunctions = activationFunction_)
    getActivationFunction() = activationFunction
    initWeights(weights_) = (weights = initiliazeWeights(weights_))
    getWeigths() = weights
    fit(inputX_,inputY_) = (weights = fitWeights(inputX_,inputY_,weights))
    train(X_,Y_,a_,epochs_) = (weights = trainModel(weights,X_,Y_,a_,activationFunctions,epochs_))
    predict(X_)= predictv(weights,X_,activationFunctions)
    predict_proba(X_)= predict_probas(weights,X_,activationFunctions)
    exportModel()=exportv(weights)
    importModel()=(weights=importv())
    () -> (setActivationFunctions;getActivationFunction;initWeights;getWeigths;fit;train;predict;predict_proba;exportModel;importModel)

end






