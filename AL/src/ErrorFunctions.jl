#ErrorFunctions module
#Implements functions that calculate the error between an actuall value and a predicted one

module ErrorFunctions
export squareofErrors

# Takes as input parameters two vetcors
# yA is the actuall values while yP is the predicted predicted ones 
# returns the error calulation based on square of difference 

function squareOfErrors(yA,yP)
    checkIfInputsHaveValues(yA,yP)
    checkIfVector(yA)
    checkIfVector(yP)
    checkIfSameLenght(yA,yP)
    return 1/2*sum((yA-yP).^2)/length(yA)
end

function checkIfInputsHaveValues(yA,yP)
    if(yA === nothing || yP === nothing)
        error("inputs should not be nothing")
    end
    if(yA === missing || yP === missing)
        error("inputs should not be missing")
    end
end

function checkIfVector(x)
    if(size(x,1)>1 && size(x,2)>1)
        error("inputs should be vetcors")
    end
end

function checkIfSameLenght(yA,yP)
    if(length(yA)!=length(yP))
     error("Inputs should be same length")   
    end
end

squareofErrors(yA,yP)=squareOfErrors(yA,yP)
end

