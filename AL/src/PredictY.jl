#module PredictY
#Predicts the output of a neural network given a map containing the weights of the layers and the Input neuron
module PredictY
export predictY
function prediY(weights,X,activationFunctions)
    validateInputs(weights,X)
    Y=X'
    for i in range(1,length(weights))
        activationFunction=activationFunctions[i]
        Y=activationFunction.(Y*weights[i])
    end
    return Y  
end

function validateInputs(weights,X)
    checkIfNoValues(weights,X)
    checkIfXIsVector(X)
    checkIfDimensionsAreValid(weights,X)
end

function checkIfNoValues(weights,X)
    if(isnothing(weights) || isnothing(X) )
        error("Inputs should not be nothing")
    end
    if(ismissing(weights) || ismissing(X))
        error("Inputs should not be missing")
    end
end


function checkIfXIsVector(X)
    if(!isa(X,Vector))
    error("X should be a vector with the input parameteres")
    end
end

function checkIfDimensionsAreValid(weights,X)
   weightsLenght=length(weights)
   if (length(X)!=size(weights[1],1))
        error("Weights dimensions don't match X input")
    end
    if (weightsLenght > 1 && sum(( size.(weights[2:weightsLenght],1) - size.(weights[1:weightsLenght-1],2)).^2) != 0 )
        error("Weights dimensions are not correct")
    end
end

predictY(weights,X,activationFunction)=prediY(weights,X,activationFunction)
end