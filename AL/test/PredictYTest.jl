#Test for module PredictY
using Test
include("../src/PredictY.jl")
activationFunction(x)=x
begin
    @testset "PredictY.predictY" begin
        @test PredictY.predictY( [[2 1; 3 4; 1 2; 3 4],[1 2; 2 3],[1,2]], [1,2,3,4], activationFunction)==363
        @test PredictY.predictY([[2 1;2 1],[1;2]], [2,1], activationFunction)==12
        @test_throws ErrorException("Inputs should not be nothing") PredictY.predictY(nothing,[2,4],activationFunction) 
        @test_throws ErrorException("Inputs should not be nothing") PredictY.predictY([2,4],nothing,activationFunction) 
        @test_throws ErrorException("Inputs should not be missing") PredictY.predictY(missing,[2,4],activationFunction) 
        @test_throws ErrorException("Inputs should not be missing") PredictY.predictY([2,4],missing,activationFunction)
        @test_throws ErrorException("X should be a vector with the input parameteres") PredictY.predictY([2,4],2,activationFunction)
        @test_throws ErrorException("Weights dimensions don't match X input") PredictY.predictY([2 4;2 1;4 1],[2,3,3,4],activationFunction)
        @test_throws ErrorException("Weights dimensions are not correct") PredictY.predictY([[2 1;2 1],[1;2],[2,1,2,3]],[2,1],activationFunction)
    end
    
end

