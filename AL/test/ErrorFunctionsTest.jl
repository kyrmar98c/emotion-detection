#Test for module ErrorFunctions
using Test
include("../src/ErrorFunctions.jl")
begin
    @testset "ErrorFunctionsTests.squareofErrors" begin
        @test ErrorFunctions.squareofErrors([1,6,7,2,2],[3,2,5,6,7]) == 6.5
        @test ErrorFunctions.squareofErrors(1,2) == 0.5
        @test_throws ErrorException("inputs should not be nothing") ErrorFunctions.squareofErrors(nothing,[2,4]) 
        @test_throws ErrorException("inputs should not be nothing") ErrorFunctions.squareofErrors([1,2,5],nothing)
        @test_throws ErrorException("inputs should not be missing") ErrorFunctions.squareofErrors(missing,[2,4]) 
        @test_throws ErrorException("inputs should not be missing") ErrorFunctions.squareofErrors([1,2,5],missing)
        @test_throws ErrorException("inputs should be vetcors")  ErrorFunctions.squareofErrors([[1,2] [3,4]],[1,4])
        @test_throws ErrorException("Inputs should be same length") ErrorFunctions.squareofErrors([1,3,4],[1,4])
    end
    
end
