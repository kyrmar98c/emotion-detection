#Test for module ANNClassifier
using Test
include("../src/ANNClassifier.jl")
foo=ANNClassifier([2,3],"sigma")
foo=fit([1,2],[1],foo)
errorFoo=ANNClassifier([2,3],"wrong")
sigmaAbsFoo=ANNClassifier([2,3],"sigmaAbs")
activation=mapActivationFunction(foo.activationFunction)
activationAbs=mapActivationFunction(sigmaAbsFoo.activationFunction)
begin
    @testset "ANNClassifier" begin
        @test typeof(foo.weights) == typeof(Vector{Array{<:Number}}())
        @test size(foo.weights[1],1)==2 && size(foo.weights[1],2)==2 && size(foo.weights[2],1)==2  && size(foo.weights[2],2)==3 && size(foo.weights[3],1)==3 && size(foo.weights[3],2)==1
        @test any(x->x<1,foo.weights[1]) && any(x->x<1,foo.weights[2]) && any(x->x<1,foo.weights[3])
        @test activation(5) == 0.9933071490757153
        @test activationAbs(-1)==0
        @test activationAbs(2)==1
        @test_throws ErrorException("please select one of the aviable activation functions sigma and sigmaAbs") mapActivationFunction(errorFoo.activationFunction)
    end
end